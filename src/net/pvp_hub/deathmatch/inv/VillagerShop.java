package net.pvp_hub.deathmatch.inv;

import net.pvp_hub.deathmatch.api.ItemUtilities;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class VillagerShop {
	
	private static Inventory k;

	public static void openFor(Player p){
		initList();
		p.openInventory(k);
	}
	
	private static void initList(){
		ItemStack p = new ItemStack(Material.STAINED_GLASS_PANE);
		ItemMeta pM = p.getItemMeta();
		pM.setDisplayName(" ");
		p.setItemMeta(pM);
		
		if(k == null){
			k = Bukkit.createInventory(null, 27, "Team-Shop");
			k.setItem(0, p);
			k.setItem(1, p);
			k.setItem(2, p);
			k.setItem(3, p);
			k.setItem(4, p);
			k.setItem(5, p);
			k.setItem(6, p);
			k.setItem(7, p);
			k.setItem(8, p);
			
			k.setItem(9, ItemUtilities.namedItem(Material.ENDER_PEARL, "�5�lEnderperle", new String[]{"�7Wirf sie und teleportiere dich an", "�7die Stelle, wo die Enderperle aufkommt.", "�cKosten: �e100 Coins"}));
			k.setItem(10, ItemUtilities.namedItem(Material.GHAST_TEAR, "�9�lUnsichtbarkeit", new String[]{"�7Benutz es und erhalte einen", "�720-sek�ndigen Unsichtbarkeitseffekt", "�cKosten: �e250 Coins"}));
			k.setItem(11, ItemUtilities.namedItem(Material.BLAZE_ROD, "�4�lHealth-Package", new String[]{"�7Benutz es und regeneriere", "�7dein Leben komplett voll", "�cKosten: �e60 Coins"}));
			
			k.setItem(18, p);
			k.setItem(19, p);
			k.setItem(20, p);
			k.setItem(21, p);
			k.setItem(22, p);
			k.setItem(23, p);
			k.setItem(24, p);
			k.setItem(25, p);
			k.setItem(26, p);
		}
	}
	
}
