package net.pvp_hub.deathmatch.inv;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ChooseTeam {

	private static Inventory kl;
	
	public static void openFor(Player p){
		initList();
		p.openInventory(kl);
	}
	
	private static void initList(){
		if(kl == null){
			kl = Bukkit.createInventory(null, 9, "�2Team-Wahl");
			
			ItemStack pl = new ItemStack(Material.STAINED_GLASS_PANE);
			ItemMeta plM = pl.getItemMeta();
			plM.setDisplayName(" ");
			pl.setItemMeta(plM);
			
			ItemStack rd = new ItemStack(Material.WOOL);
			rd.setDurability((short) 14);
			ItemMeta rdM = rd.getItemMeta();
			rdM.setDisplayName("�4Rotes Team");
			ArrayList<String> rdL = new ArrayList<String>();
			rdL.add("�7Klicke um im �4roten �7Team zu spielen.");
			rdL.add("�cDiese Team-Wahl kostet dich 1 Chip.");
			rdM.setLore(rdL);
			rd.setItemMeta(rdM);
			
			ItemStack bl = new ItemStack(Material.WOOL);
			bl.setDurability((short) 11);
			ItemMeta blM = bl.getItemMeta();
			blM.setDisplayName("�9Blaues Team");
			ArrayList<String> blL = new ArrayList<String>();
			blL.add("�7Klicke um im �9blauen �7Team zu spielen.");
			blL.add("�cDiese Team-Wahl kostet dich 1 Chip.");
			rdM.setLore(blL);
			bl.setItemMeta(blM);
			
			ItemStack gr = new ItemStack(Material.WOOL);
			gr.setDurability((short) 5);
			ItemMeta grM = gr.getItemMeta();
			grM.setDisplayName("�2Gr�nes Team");
			ArrayList<String> grL = new ArrayList<String>();
			grL.add("�7Klicke um im �2gr�nen �7Team zu spielen.");
			grL.add("�cDiese Team-Wahl kostet dich 1 Chip.");
			grM.setLore(grL);
			gr.setItemMeta(grM);
			
			kl.setItem(0, pl);
			kl.setItem(1, pl);
			kl.setItem(2, rd);
			kl.setItem(3, pl);
			kl.setItem(4, bl);
			kl.setItem(5, pl);
			kl.setItem(6, gr);
			kl.setItem(7, pl);
			kl.setItem(8, pl);
		}
	}
	
}
