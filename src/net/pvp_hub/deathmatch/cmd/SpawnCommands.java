package net.pvp_hub.deathmatch.cmd;

import java.io.IOException;

import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.deathmatch.Deathmatch;

import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpawnCommands implements CommandExecutor {

	private Deathmatch plugin;
	public SpawnCommands(Deathmatch plugin){
		this.plugin = plugin;
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("setspawnred")){
			if((sender instanceof Player)){
				if(sender.isOp()){
					Player p = (Player)sender;
					
					World w = p.getLocation().getWorld();
					double x = p.getLocation().getX();
					double y = p.getLocation().getY();
					double z = p.getLocation().getZ();
					float yaw = p.getLocation().getYaw();
					float pitch = p.getLocation().getPitch();
					
					p.sendMessage(plugin.prefix + "�aX: �e" + x);
					p.sendMessage(plugin.prefix + "�aY: �e" + y);
					p.sendMessage(plugin.prefix + "�aZ: �e" + z);
					p.sendMessage(plugin.prefix + "�aYaw: �e" + yaw);
					p.sendMessage(plugin.prefix + "�aPitch: �e" + pitch);
					
					plugin.cfg.set("DM.Spawns.Red.World", w.getName());
					plugin.cfg.set("DM.Spawns.Red.X", x);
					plugin.cfg.set("DM.Spawns.Red.Y", y);
					plugin.cfg.set("DM.Spawns.Red.Z", z);
					plugin.cfg.set("DM.Spawns.Red.Yaw", yaw);
					plugin.cfg.set("DM.Spawns.Red.Pitch", pitch);
					try {
						plugin.cfg.save(plugin.file);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("setspawnblue")){
			if((sender instanceof Player)){
				if(sender.isOp()){
					Player p = (Player)sender;
					
					World w = p.getLocation().getWorld();
					double x = p.getLocation().getX();
					double y = p.getLocation().getY();
					double z = p.getLocation().getZ();
					float yaw = p.getLocation().getYaw();
					float pitch = p.getLocation().getPitch();
					
					p.sendMessage(plugin.prefix + "�aX: �e" + x);
					p.sendMessage(plugin.prefix + "�aY: �e" + y);
					p.sendMessage(plugin.prefix + "�aZ: �e" + z);
					p.sendMessage(plugin.prefix + "�aYaw: �e" + yaw);
					p.sendMessage(plugin.prefix + "�aPitch: �e" + pitch);
					
					plugin.cfg.set("DM.Spawns.Blue.World", w.getName());
					plugin.cfg.set("DM.Spawns.Blue.X", x);
					plugin.cfg.set("DM.Spawns.Blue.Y", y);
					plugin.cfg.set("DM.Spawns.Blue.Z", z);
					plugin.cfg.set("DM.Spawns.Blue.Yaw", yaw);
					plugin.cfg.set("DM.Spawns.Blue.Pitch", pitch);
					try {
						plugin.cfg.save(plugin.file);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("setspawngreen")){
			if((sender instanceof Player)){
				if(sender.isOp()){
					Player p = (Player)sender;
					
					World w = p.getLocation().getWorld();
					double x = p.getLocation().getX();
					double y = p.getLocation().getY();
					double z = p.getLocation().getZ();
					float yaw = p.getLocation().getYaw();
					float pitch = p.getLocation().getPitch();
					
					p.sendMessage(plugin.prefix + "�aX: �e" + x);
					p.sendMessage(plugin.prefix + "�aY: �e" + y);
					p.sendMessage(plugin.prefix + "�aZ: �e" + z);
					p.sendMessage(plugin.prefix + "�aYaw: �e" + yaw);
					p.sendMessage(plugin.prefix + "�aPitch: �e" + pitch);
					
					plugin.cfg.set("DM.Spawns.Green.World", w.getName());
					plugin.cfg.set("DM.Spawns.Green.X", x);
					plugin.cfg.set("DM.Spawns.Green.Y", y);
					plugin.cfg.set("DM.Spawns.Green.Z", z);
					plugin.cfg.set("DM.Spawns.Green.Yaw", yaw);
					plugin.cfg.set("DM.Spawns.Green.Pitch", pitch);
					try {
						plugin.cfg.save(plugin.file);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("villgreen")){
			if((sender instanceof Player)){
				if(sender.isOp()){
					Player p = (Player)sender;
					
					World w = p.getLocation().getWorld();
					double x = p.getLocation().getX();
					double y = p.getLocation().getY();
					double z = p.getLocation().getZ();
					float yaw = p.getLocation().getYaw();
					float pitch = p.getLocation().getPitch();
					
					p.sendMessage(plugin.prefix + "�aX: �e" + x);
					p.sendMessage(plugin.prefix + "�aY: �e" + y);
					p.sendMessage(plugin.prefix + "�aZ: �e" + z);
					p.sendMessage(plugin.prefix + "�aYaw: �e" + yaw);
					p.sendMessage(plugin.prefix + "�aPitch: �e" + pitch);
					
					plugin.cfg.set("DM.Spawns.Green.World", w.getName());
					plugin.cfg.set("DM.Spawns.Green.X", x);
					plugin.cfg.set("DM.Spawns.Green.Y", y);
					plugin.cfg.set("DM.Spawns.Green.Z", z);
					plugin.cfg.set("DM.Spawns.Green.Yaw", yaw);
					plugin.cfg.set("DM.Spawns.Green.Pitch", pitch);
					try {
						plugin.cfg.save(plugin.file);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("villblue")){
			if((sender instanceof Player)){
				if(sender.isOp()){
					Player p = (Player)sender;
					
					World w = p.getLocation().getWorld();
					double x = p.getLocation().getX();
					double y = p.getLocation().getY();
					double z = p.getLocation().getZ();
					float yaw = p.getLocation().getYaw();
					float pitch = p.getLocation().getPitch();
					
					p.sendMessage(plugin.prefix + "�aX: �e" + x);
					p.sendMessage(plugin.prefix + "�aY: �e" + y);
					p.sendMessage(plugin.prefix + "�aZ: �e" + z);
					p.sendMessage(plugin.prefix + "�aYaw: �e" + yaw);
					p.sendMessage(plugin.prefix + "�aPitch: �e" + pitch);
					
					plugin.cfg.set("DM.Villagers.Blue.World", w.getName());
					plugin.cfg.set("DM.Villagers.Blue.X", x);
					plugin.cfg.set("DM.Villagers.Blue.Y", y);
					plugin.cfg.set("DM.Villagers.Blue.Z", z);
					plugin.cfg.set("DM.Villagers.Blue.Yaw", yaw);
					plugin.cfg.set("DM.Villagers.Blue.Pitch", pitch);
					try {
						plugin.cfg.save(plugin.file);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("villred")){
			if((sender instanceof Player)){
				if(sender.isOp()){
					Player p = (Player)sender;
					
					World w = p.getLocation().getWorld();
					double x = p.getLocation().getX();
					double y = p.getLocation().getY();
					double z = p.getLocation().getZ();
					float yaw = p.getLocation().getYaw();
					float pitch = p.getLocation().getPitch();
					
					p.sendMessage(plugin.prefix + "�aX: �e" + x);
					p.sendMessage(plugin.prefix + "�aY: �e" + y);
					p.sendMessage(plugin.prefix + "�aZ: �e" + z);
					p.sendMessage(plugin.prefix + "�aYaw: �e" + yaw);
					p.sendMessage(plugin.prefix + "�aPitch: �e" + pitch);
					
					plugin.cfg.set("DM.Villagers.Red.World", w.getName());
					plugin.cfg.set("DM.Villagers.Red.X", x);
					plugin.cfg.set("DM.Villagers.Red.Y", y);
					plugin.cfg.set("DM.Villagers.Red.Z", z);
					plugin.cfg.set("DM.Villagers.Red.Yaw", yaw);
					plugin.cfg.set("DM.Villagers.Red.Pitch", pitch);
					try {
						plugin.cfg.save(plugin.file);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				sender.sendMessage(PluginMeta.noplayer);
			}
		}
		
		return false;
	}
	
}
