package net.pvp_hub.deathmatch.cmd;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.deathmatch.Deathmatch;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Stats implements CommandExecutor {

	private Deathmatch plugin;
	public Stats(Deathmatch plugin){
		this.plugin = plugin;
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		final Player p = (Player)sender;
		
		if(cmd.getName().equalsIgnoreCase("stats")){
			if(args.length == 0){
				if((sender instanceof Player)){
					Player pl = (Player)sender;
					String pn = pl.getName();
					
					try {
						sender.sendMessage(Deathmatch.prefix + "�3========= �eStats von " + pl.getDisplayName() + " �3=========");
						sender.sendMessage(Deathmatch.prefix + "�6Points: �e" + Deathmatch.getPoints(pn));
						sender.sendMessage(Deathmatch.prefix + "�6Kills: �e" + Deathmatch.getKills(pn));
						sender.sendMessage(Deathmatch.prefix + "�6Deaths: �e" + Deathmatch.getDeaths(pn));
						sender.sendMessage(Deathmatch.prefix + "�6Gewonnene Spiele: �e" + Deathmatch.getVictories(pn));
						sender.sendMessage(Deathmatch.prefix + "�6Gespielte Spiele: �e" + Deathmatch.getPlayedGames(pn));
						sender.sendMessage(Deathmatch.prefix + "�6Get�tete Villager: �e" + Deathmatch.getKilledVillagers(pn));
						//sender.sendMessage(Deathmatch.prefix + "�6K/D: �e" + Deathmatch.getKD(pn));
						//sender.sendMessage(Deathmatch.prefix + "�6H�chster Killstreak: �e" + this.getHighestStreak(p.getName()));
						sender.sendMessage(Deathmatch.prefix + "�3========= �eStats von " + pl.getDisplayName() + " �3=========");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					sender.sendMessage(PluginMeta.noplayer);
				}
			} else {
				if(PlayerUtilities.isOnline(args[0])){
					Player pl = Bukkit.getServer().getPlayer(args[0]);
					
					try {
						sender.sendMessage(Deathmatch.prefix + "�3========= �eStats von " + pl.getDisplayName() + " �3=========");
						sender.sendMessage(Deathmatch.prefix + "�6Points: �e" + Deathmatch.getPoints(pl.getName()));
						sender.sendMessage(Deathmatch.prefix + "�6Kills: �e" + Deathmatch.getKills(pl.getName()));
						sender.sendMessage(Deathmatch.prefix + "�6Deaths: �e" + Deathmatch.getDeaths(pl.getName()));
						sender.sendMessage(Deathmatch.prefix + "�6Gewonnene Spiele: �e" + Deathmatch.getVictories(pl.getName()));
						sender.sendMessage(Deathmatch.prefix + "�6Gespielte Spiele: �e" + Deathmatch.getPlayedGames(pl.getName()));
						sender.sendMessage(Deathmatch.prefix + "�6Get�tete Villager: �e" + Deathmatch.getKilledVillagers(pl.getName()));
						//sender.sendMessage(Deathmatch.prefix + "�6K/D: �e" + Deathmatch.getKD(pl.getName()));
						//sender.sendMessage(Deathmatch.prefix + "�6H�chster Killstreak: �e" + this.getHighestStreak(p.getName()));
						sender.sendMessage(Deathmatch.prefix + "�3========= �eStats von " + pl.getDisplayName() + " �3=========");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					OfflinePlayer pl = Bukkit.getServer().getOfflinePlayer(args[0]);
					try {
						String col = "�7";
						int r = PlayerUtilities.getRank(pl.getName());
						
						if(r == 0){
							col = "�7";
						} else if(r == 1){
							col = "�6";
						} else if(r == 2){
							col = "�6";
						} else if(r == 3){
							col = "�5";
						} else if(r == 4){
							col = "�5";
						} else if(r == 5){
							col = "�b";
						} else if(r == 6){
							col = "�3";
						} else if(r == 7){
							col = "�a";
						} else if(r == 8){
							col = "�e";
						} else if(r == 9){
							col = "�9";
						} else if(r == 10){
							col = "�c";
						} else if(r == 11){
							col = "�c";
						}
						
						sender.sendMessage(Deathmatch.prefix + "�3========= �eStats von " + col + pl.getName() + " �3=========");
						sender.sendMessage(Deathmatch.prefix + "�6Punkte: �e" + Deathmatch.getPoints(pl.getName()));
						sender.sendMessage(Deathmatch.prefix + "�6Kills: �e" + Deathmatch.getKills(pl.getName()));
						sender.sendMessage(Deathmatch.prefix + "�6Deaths: �e" + Deathmatch.getDeaths(pl.getName()));
						sender.sendMessage(Deathmatch.prefix + "�6Gewonnene Spiele: �e" + Deathmatch.getVictories(pl.getName()));
						sender.sendMessage(Deathmatch.prefix + "�6Gespielte Spiele: �e" + Deathmatch.getPlayedGames(pl.getName()));
						sender.sendMessage(Deathmatch.prefix + "�6Get�tete Villager: �e" + Deathmatch.getKilledVillagers(pl.getName()));
						//sender.sendMessage(Deathmatch.prefix + "�6K/D: �e" + Deathmatch.getKD(pl.getName()));
						//sender.sendMessage(Vars.prefix + "�6H�chster Killstreak: �e" + Deathmatch.getHighestStreak(p.getName()));
						sender.sendMessage(Deathmatch.prefix + "�3========= �eStats von " + col + pl.getName() + " �3=========");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		
		return false;
	}
	
}
