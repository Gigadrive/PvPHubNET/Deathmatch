package net.pvp_hub.deathmatch.cmd;

import java.io.IOException;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.deathmatch.Deathmatch;
import net.pvp_hub.deathmatch.api.GameUtilities;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SoftStop implements CommandExecutor {

	private Deathmatch plugin;
	public SoftStop(Deathmatch plugin){
		this.plugin = plugin;
	}
	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		final Player p = (Player)sender;
		
		if(cmd.getName().equalsIgnoreCase("softstop")){
			try {
				if(PlayerUtilities.getRank(p.getName()) > 8 || PlayerUtilities.getRank(p.getName()) == 8){
					
					for(Player all : Bukkit.getOnlinePlayers()){
						all.sendMessage(PluginMeta.prefix + "�cDas Spiel wurde von einem Admin manuell beendet.");
						try {
							PlayerUtilities.connect(Deathmatch.getInstance(), "lobby", all);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					
					for(Player all : Bukkit.getOnlinePlayers()){
						PlayerUtilities.clearInventory(all);
						all.teleport(Bukkit.getWorld("Lobby").getSpawnLocation());
					}
					
					GameUtilities.refillDiamond();
					GameUtilities.refillGold();
					GameUtilities.refillRedstone();
					
					Bukkit.reload();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return false;
		
	}

}
