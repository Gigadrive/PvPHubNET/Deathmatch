package net.pvp_hub.deathmatch;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_7_R4.ChatSerializer;
import net.minecraft.server.v1_7_R4.IChatBaseComponent;
import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.GameState;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.deathmatch.api.GameUtilities;
import net.pvp_hub.deathmatch.api.TeamUtilities;
import net.pvp_hub.deathmatch.cmd.SoftStop;
import net.pvp_hub.deathmatch.cmd.SpawnCommands;
import net.pvp_hub.deathmatch.cmd.Stats;
import net.pvp_hub.deathmatch.listener.AsyncPlayerChatListener;
import net.pvp_hub.deathmatch.listener.BlockBreakListener;
import net.pvp_hub.deathmatch.listener.BlockPlaceListener;
import net.pvp_hub.deathmatch.listener.CreatureSpawnListener;
import net.pvp_hub.deathmatch.listener.EntityDamageByEntityListener;
import net.pvp_hub.deathmatch.listener.EntityDamageListener;
import net.pvp_hub.deathmatch.listener.EntityDeathListener;
import net.pvp_hub.deathmatch.listener.EntityInteractListener;
import net.pvp_hub.deathmatch.listener.FoodLevelChangeListener;
import net.pvp_hub.deathmatch.listener.InventoryClickListener;
import net.pvp_hub.deathmatch.listener.PlayerDeathListener;
import net.pvp_hub.deathmatch.listener.PlayerInteractListener;
import net.pvp_hub.deathmatch.listener.PlayerJoinListener;
import net.pvp_hub.deathmatch.listener.PlayerQuitListener;
import net.pvp_hub.deathmatch.listener.PlayerRespawnListener;
import net.pvp_hub.deathmatch.listener.ServerPingListener;
import net.pvp_hub.deathmatch.listener.WeatherChangeListener;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;
import org.kitteh.tag.AsyncPlayerReceiveNameTagEvent;
import org.kitteh.tag.TagAPI;
import org.spigotmc.ProtocolInjector.PacketTitle;
import org.spigotmc.ProtocolInjector.PacketTitle.Action;

import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.MySQLData;
import tk.theakio.sql.QueryResult;
import tk.theakio.sql.SQLHandler;

public class Deathmatch extends JavaPlugin implements Listener {
	
	public static final String prefix = "�3[�bDeathMatch�3] �6";
	public static final String prefixLog = "[DeathMatch]";
	public static final String prefixLogIngame = prefix + "�4[�cDEBUG�4] �6";
	public static HashMap<Player, Integer> teams = new HashMap<Player, Integer>();
	
	public static File file = new File("plugins/DeathMatch", "config.yml");
	public static FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	
	public static String gameState = null;
	public static String mapBuilder = null;
	public static String mapName = null;
	public static String mapLink = null;
	public static String mapMotd = null;
	
	private QueryResult qr = null;
	public String main = "main";
	
	public static boolean friendly;
	public static boolean joinable;
	public static boolean redIsKilled;
	public static boolean blueIsKilled;
	public static boolean greenIsKilled;
	
	public static ScoreboardManager manager = Bukkit.getScoreboardManager();
	public static Scoreboard board = manager.getNewScoreboard();
	public static Team blueTeam = board.registerNewTeam("BLUE");
	public static Team redTeam = board.registerNewTeam("RED");
	public static Team greenTeam = board.registerNewTeam("GREEN");
	public static Objective obj = board.registerNewObjective("bla", "dummy");
	public static Score greenScore = obj.getScore("�2Team GREEN");
	public static Score redScore = obj.getScore("�4Team RED");
	public static Score blueScore = obj.getScore("�9Team BLUE");
	
	public int lobbyCount = 61;
	
	public static Deathmatch instance;
	
	public static List<Team> killedVillagers = new LinkedList<Team>();
	
	public Deathmatch() {
	   instance = this;
	}
	
	public static Deathmatch getInstance() {
	   return instance;
	}

	public void onEnable(){
		World w = Bukkit.getWorld(cfg.getString("DM.Spawns.Red.World"));
		for(Entity e : w.getEntities()){
			e.remove();
		}
		
		SQLHandler Handler = HandlerStorage.addHandler(main, new SQLHandler());
		Handler.setMySQLOptions(MySQLData.mysql_u_host, MySQLData.mysql_u_port, MySQLData.mysql_u_user, MySQLData.mysql_u_pass, MySQLData.mysql_u_database);
		try {
			Handler.openConnection();
		} catch (Exception e) {
			System.err.println("[MySQL API von Akio Zeugs] Putt Putt!");
		}
		
		System.out.println(prefixLog + "ENABLED");
		
		TeamUtilities.teams.add(blueTeam);
		TeamUtilities.teams.add(greenTeam);
		TeamUtilities.teams.add(redTeam);
	    
		blueTeam.setPrefix(ChatColor.BLUE.toString());
	    blueTeam.setDisplayName("�9Blaues Team");
	    blueTeam.setCanSeeFriendlyInvisibles(true);
	    blueTeam.setAllowFriendlyFire(false);
	    
	    redTeam.setPrefix(ChatColor.DARK_RED.toString());
	    redTeam.setDisplayName("�4Rotes Team");
	    redTeam.setCanSeeFriendlyInvisibles(true);
	    redTeam.setAllowFriendlyFire(false);
	    
	    greenTeam.setPrefix(ChatColor.DARK_GREEN.toString());
	    greenTeam.setDisplayName("�2Gr�nes Team");
	    greenTeam.setCanSeeFriendlyInvisibles(true);
	    greenTeam.setAllowFriendlyFire(false);
	    
	    obj.setDisplayName("�3DM �8| �cFIRST-TO-12");
	    obj.setDisplaySlot(DisplaySlot.SIDEBAR);
	    
	    greenScore.setScore(0);
	    blueScore.setScore(0);
	    redScore.setScore(0);
		
		mapBuilder = cfg.getString("Map.mapBuilder");
		mapName = cfg.getString("Map.mapName");
		mapLink = cfg.getString("Map.mapLink");
		mapMotd = cfg.getString("Map.mapMotd");
		
		friendly = true;
		joinable = true;
		
		redIsKilled = false;
		greenIsKilled = false;
		blueIsKilled = false;
		
		Location vilRed = new Location(Bukkit.getWorld(cfg.getString("DM.Villagers.Red.World")), cfg.getDouble("DM.Villagers.Red.X"), cfg.getDouble("DM.Villagers.Red.Y"), cfg.getDouble("DM.Villagers.Red.Z"), (float)cfg.getInt("DM.Villagers.Red.Yaw"), (float)cfg.getInt("DM.Villagers.Red.Pitch"));
		Location vilBlue = new Location(Bukkit.getWorld(cfg.getString("DM.Villagers.Blue.World")), cfg.getDouble("DM.Villagers.Blue.X"), cfg.getDouble("DM.Villagers.Blue.Y"), cfg.getDouble("DM.Villagers.Blue.Z"), (float)cfg.getInt("DM.Villagers.Blue.Yaw"), (float)cfg.getInt("DM.Villagers.Blue.Pitch"));
		Location vilGreen = new Location(Bukkit.getWorld(cfg.getString("DM.Villagers.Green.World")), cfg.getDouble("DM.Villagers.Green.X"), cfg.getDouble("DM.Villagers.Green.Y"), cfg.getDouble("DM.Villagers.Green.Z"), (float)cfg.getInt("DM.Villagers.Green.Yaw"), (float)cfg.getInt("DM.Villagers.Green.Pitch"));
		
		Villager vilRedEn = (Villager)w.spawnEntity(vilRed, EntityType.VILLAGER);
		vilRedEn.setCustomName("�4[ �cTeam-Shop �4]");
		vilRedEn.setCustomNameVisible(true);
		
		Villager vilBlueEn = (Villager)w.spawnEntity(vilBlue, EntityType.VILLAGER);
		vilBlueEn.setCustomName("�9[ �3Team-Shop �9]");
		vilBlueEn.setCustomNameVisible(true);
		
		Villager vilGreenEn = (Villager)w.spawnEntity(vilGreen, EntityType.VILLAGER);
		vilGreenEn.setCustomName("�2[ �aTeam-Shop �2]");
		vilGreenEn.setCustomNameVisible(true);
		
		gameState = "�a�lLobby";
		
		Bukkit.getPluginManager().registerEvents(this, this);
		Bukkit.getPluginManager().registerEvents(new AsyncPlayerChatListener(this), this);
		Bukkit.getPluginManager().registerEvents(new BlockBreakListener(this), this);
		Bukkit.getPluginManager().registerEvents(new BlockPlaceListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerInteractListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerQuitListener(this), this);
		Bukkit.getPluginManager().registerEvents(new CreatureSpawnListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerDeathListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerRespawnListener(this), this);
		Bukkit.getPluginManager().registerEvents(new FoodLevelChangeListener(this), this);
		Bukkit.getPluginManager().registerEvents(new EntityDamageListener(this), this);
		Bukkit.getPluginManager().registerEvents(new EntityDamageByEntityListener(this), this);
		Bukkit.getPluginManager().registerEvents(new EntityDeathListener(this), this);
		Bukkit.getPluginManager().registerEvents(new EntityInteractListener(this), this);
		Bukkit.getPluginManager().registerEvents(new InventoryClickListener(this), this);
		Bukkit.getPluginManager().registerEvents(new WeatherChangeListener(this), this);
		//Bukkit.getPluginManager().registerEvents(new ServerPingListener(this), this);
		
		getCommand("setspawnred").setExecutor(new SpawnCommands(this));
		getCommand("setspawngreen").setExecutor(new SpawnCommands(this));
		getCommand("setspawnblue").setExecutor(new SpawnCommands(this));
		
		getCommand("villred").setExecutor(new SpawnCommands(this));
		getCommand("villblue").setExecutor(new SpawnCommands(this));
		getCommand("villgreen").setExecutor(new SpawnCommands(this));
		
		getCommand("stats").setExecutor(new Stats(this));
		getCommand("softstop").setExecutor(new SoftStop(this));
		
		getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		
		PvPHubCore.setGameState(GameState.LOBBY);
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){
			@SuppressWarnings("deprecation")
			public void run(){
				lobbyCount--;
				for(Player all : Bukkit.getOnlinePlayers()){
					all.setExp((float) ((double) lobbyCount / 60D));
					all.setLevel(lobbyCount);
				}
				if(lobbyCount == 60){
					Bukkit.broadcastMessage(prefix + "Noch �b" + lobbyCount + " �6Sekunden");
				} else if(lobbyCount == 50){
					Bukkit.broadcastMessage(prefix + "Noch �b" + lobbyCount + " �6Sekunden");
				} else if(lobbyCount == 40){
					Bukkit.broadcastMessage(prefix + "Noch �b" + lobbyCount + " �6Sekunden");
				} else if(lobbyCount == 30){
					Bukkit.broadcastMessage(prefix + "Noch �b" + lobbyCount + " �6Sekunden");
				} else if(lobbyCount == 20){
					Bukkit.broadcastMessage(prefix + "Noch �b" + lobbyCount + " �6Sekunden");
				} else if(lobbyCount == 10){
					Bukkit.broadcastMessage(prefix + "Noch �b" + lobbyCount + " �6Sekunden");
				} else if(lobbyCount == 9){
					Bukkit.broadcastMessage(prefix + "Noch �b" + lobbyCount + " �6Sekunden");
				} else if(lobbyCount == 8){
					Bukkit.broadcastMessage(prefix + "Noch �b" + lobbyCount + " �6Sekunden");
				} else if(lobbyCount == 7){
					Bukkit.broadcastMessage(prefix + "Noch �b" + lobbyCount + " �6Sekunden");
				} else if(lobbyCount == 6){
					Bukkit.broadcastMessage(prefix + "Noch �b" + lobbyCount + " �6Sekunden");
				} else if(lobbyCount == 5){
					Bukkit.broadcastMessage(prefix + "Noch �b" + lobbyCount + " �6Sekunden");
				} else if(lobbyCount == 4){
					Bukkit.broadcastMessage(prefix + "Noch �b" + lobbyCount + " �6Sekunden");
				} else if(lobbyCount == 3){
					Bukkit.broadcastMessage(prefix + "Noch �b" + lobbyCount + " �6Sekunden");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getEyeLocation(), Sound.NOTE_BASS, 1.0F, 1.0F);
					}
				} else if(lobbyCount == 2){
					Bukkit.broadcastMessage(prefix + "Noch �b" + lobbyCount + " �6Sekunden");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getEyeLocation(), Sound.NOTE_BASS, 1.0F, 1.0F);
					}
				} else if(lobbyCount == 1){
					Bukkit.broadcastMessage(prefix + "Noch �b" + lobbyCount + " �6Sekunde");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getEyeLocation(), Sound.NOTE_BASS, 1.0F, 1.0F);
					}
				} else if(lobbyCount == 0){
					if(Bukkit.getOnlinePlayers().length < 3){
						lobbyCount = 61;
						Bukkit.broadcastMessage(prefix + "�cZu wenig Spieler online.. (mind. 3)");
						for(Player all : Bukkit.getOnlinePlayers()){
							all.playSound(all.getEyeLocation(), Sound.NOTE_BASS, 1.0F, 0.5F);
						}
					} else {
						//PlayerUtilities.removeAllFromTeams();
						Bukkit.broadcastMessage(prefix + "Das Spiel beginnt!");
						PvPHubCore.setGameState(GameState.INGAME);
						for (final Player all : Bukkit.getOnlinePlayers()){
							TeamUtilities.shuffleIntoTeams();
							Bukkit.getScheduler().scheduleSyncDelayedTask(Deathmatch.getInstance(), new Runnable(){
								public void run(){
									shuffleChosenPlayers();
									all.setDisplayName(TeamUtilities.getTeamColor(TeamUtilities.getTeamByPlayer(all)) + all.getName());
									all.setPlayerListName(PlayerUtilities.cutDisplayName(all.getDisplayName()));
									TeamUtilities.teleportOnMap(all, TeamUtilities.getTeamByPlayer(all));
									all.playSound(all.getEyeLocation(), Sound.NOTE_BASS, 1.0F, 2.0F);
									all.setScoreboard(board);
									all.setHealth(20.0);
									all.setFireTicks(0);
									all.setFoodLevel(20);
									// TODO sounds
									PlayerUtilities.clearInventory(all);
									all.getInventory().setItem(0, new ItemStack(Material.GOLD_SWORD));
									
									try {
										Deathmatch.addPlayedGames(all.getName());
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									
									if(((CraftPlayer) all).getHandle().playerConnection.networkManager.getVersion() >= 47){
										IChatBaseComponent chatTitle = ChatSerializer.a("{\"text\": \"�eMap: �3" + mapName + "\"}");
										PacketTitle title = new PacketTitle(Action.TITLE, chatTitle);
										((CraftPlayer) all).getHandle().playerConnection.sendPacket(title);
										
										IChatBaseComponent subTitle = ChatSerializer.a("{\"text\": \"�eby �3" + mapBuilder + "\"}");
										PacketTitle stitle = new PacketTitle(Action.SUBTITLE, subTitle);
										((CraftPlayer) all).getHandle().playerConnection.sendPacket(stitle);
									}
									
									try {
										if(PlayerUtilities.hasBoughtShopItem(all, 20)){
											all.getInventory().setHelmet(new ItemStack(Material.LEATHER_HELMET));
											all.getInventory().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
											all.getInventory().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
											all.getInventory().setBoots(new ItemStack(Material.LEATHER_BOOTS));
										}
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							}, 40L);
								
						}
						
						World w = Bukkit.getWorld(Deathmatch.cfg.getString("DM.Spawns.Red.World"));
						w.setPVP(true);
						w.setDifficulty(Difficulty.EASY);
						w.setGameRuleValue("naturalRegeneration", "true");
						
						gameState = "�4�lIngame";
						
						friendly = false;
						joinable = false;
						
						for(Player all : Bukkit.getOnlinePlayers()){
							TagAPI.refreshPlayer(all);
						}
						
						refillScheduler("red");
						refillScheduler("gold");
						respawnScheduler();
					}
				}
			}
		}, 20L, 20L);
	}
	
	public void onDisable(){}
	
	public void refillScheduler(String w){
		
		if(w == "red"){
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){

				@Override
				public void run() {
					GameUtilities.refillRedstone();
				}
				
			}, 1 * 20, 180 * 20);
		} else if (w == "gold"){
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){

				@Override
				public void run() {
					GameUtilities.refillGold();
				}
				
			}, 1 * 20, 300 * 20);
		}
	}
	
	public void shuffleChosenPlayers(){
		for(Player all : TeamUtilities.playersForBlue){
			TeamUtilities.removePlayerFromTeam(all);
			blueTeam.addPlayer(all);
		}
		
		for(Player all : TeamUtilities.playersForGreen){
			TeamUtilities.removePlayerFromTeam(all);
			blueTeam.addPlayer(all);
		}
		
		for(Player all : TeamUtilities.playersForRed){
			TeamUtilities.removePlayerFromTeam(all);
			blueTeam.addPlayer(all);
		}
	}
	
	public void respawnScheduler(){
		final World w = Bukkit.getWorld(cfg.getString("DM.Spawns.Red.World"));
		final Location vilRed = new Location(Bukkit.getWorld(cfg.getString("DM.Villagers.Red.World")), cfg.getDouble("DM.Villagers.Red.X"), cfg.getDouble("DM.Villagers.Red.Y"), cfg.getDouble("DM.Villagers.Red.Z"), (float)cfg.getInt("DM.Villagers.Red.Yaw"), (float)cfg.getInt("DM.Villagers.Red.Pitch"));
		final Location vilBlue = new Location(Bukkit.getWorld(cfg.getString("DM.Villagers.Blue.World")), cfg.getDouble("DM.Villagers.Blue.X"), cfg.getDouble("DM.Villagers.Blue.Y"), cfg.getDouble("DM.Villagers.Blue.Z"), (float)cfg.getInt("DM.Villagers.Blue.Yaw"), (float)cfg.getInt("DM.Villagers.Blue.Pitch"));
		final Location vilGreen = new Location(Bukkit.getWorld(cfg.getString("DM.Villagers.Green.World")), cfg.getDouble("DM.Villagers.Green.X"), cfg.getDouble("DM.Villagers.Green.Y"), cfg.getDouble("DM.Villagers.Green.Z"), (float)cfg.getInt("DM.Villagers.Green.Yaw"), (float)cfg.getInt("DM.Villagers.Green.Pitch"));
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){

			@Override
			public void run() {
				/*for(Team t : killedVillagers){
					if(t.equals(redTeam)){
						Villager vilRedEn = (Villager)w.spawnEntity(vilRed, EntityType.VILLAGER);
						vilRedEn.setCustomName("�4[ �cTeam-Shop �4]");
						vilRedEn.setCustomNameVisible(true);
						
						for(OfflinePlayer all : redTeam.getPlayers()){
							Player a = (Player)all;
							a.sendMessage(prefix + "�aDein Villager wurde neugespawnt!");
						}
					}
					
					if(t.equals(blueTeam)){
						Villager vilBlueEn = (Villager)w.spawnEntity(vilBlue, EntityType.VILLAGER);
						vilBlueEn.setCustomName("�9[ �3Team-Shop �9]");
						vilBlueEn.setCustomNameVisible(true);
						
						for(OfflinePlayer all : blueTeam.getPlayers()){
							Player a = (Player)all;
							a.sendMessage(prefix + "�aDein Villager wurde neugespawnt!");
						}
					}
					
					if(t.equals(greenTeam)){
						Villager vilGreenEn = (Villager)w.spawnEntity(vilGreen, EntityType.VILLAGER);
						vilGreenEn.setCustomName("�2[ �aTeam-Shop �2]");
						vilGreenEn.setCustomNameVisible(true);
						
						for(OfflinePlayer all : greenTeam.getPlayers()){
							Player a = (Player)all;
							a.sendMessage(prefix + "�aDein Villager wurde neugespawnt!");
						}
					}
				}*/
				
				if(redIsKilled){
					Villager vilRedEn = (Villager)w.spawnCreature(vilRed, EntityType.VILLAGER);
					vilRedEn.setCustomName("�4[ �cTeam-Shop �4]");
					vilRedEn.setCustomNameVisible(true);
					
					for(OfflinePlayer all : redTeam.getPlayers()){
						Player a = (Player)all;
						a.sendMessage(prefix + "�aDein Villager wurde neu gespawnt!");
					}
					
					redIsKilled = false;
				}
				
				if(blueIsKilled){
					Villager vilBlueEn = (Villager)w.spawnEntity(vilBlue, EntityType.VILLAGER);
					vilBlueEn.setCustomName("�9[ �3Team-Shop �9]");
					vilBlueEn.setCustomNameVisible(true);
					
					for(OfflinePlayer all : blueTeam.getPlayers()){
						Player a = (Player)all;
						a.sendMessage(prefix + "�aDein Villager wurde neu gespawnt!");
					}
					
					blueIsKilled = false;
				}
				
				if(greenIsKilled){
					Villager vilGreenEn = (Villager)w.spawnEntity(vilGreen, EntityType.VILLAGER);
					vilGreenEn.setCustomName("�2[ �aTeam-Shop �2]");
					vilGreenEn.setCustomNameVisible(true);
					
					for(OfflinePlayer all : greenTeam.getPlayers()){
						Player a = (Player)all;
						a.sendMessage(prefix + "�aDein Villager wurde neu gespawnt!");
					}
					
					greenIsKilled = false;
				}
				
				
			}
			
		}, 300 * 20, 300 * 20);
	}
	
	@EventHandler
	public void onReceiveNameTag(AsyncPlayerReceiveNameTagEvent e){
		if(!this.joinable){
			e.setTag(TeamUtilities.getTeamColor(TeamUtilities.getTeamByPlayer(e.getNamedPlayer())) + e.getNamedPlayer().getName());
		}
	}
	
	public static int getPoints(String p) throws Exception {
		int points = 100;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM dm_stats WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			points = qr.rs.getInt("points");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return points;
	}
	
	public static int getKills(String p) throws Exception {
		int kills = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM dm_stats WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			kills = qr.rs.getInt("kills");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return kills;
	}
	
	public static int getDeaths(String p) throws Exception {
		int deaths = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM dm_stats WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			deaths = qr.rs.getInt("deaths");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return deaths;
	}
	
	public static int getKilledVillagers(String p) throws Exception {
		int killedVillagers = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM dm_stats WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			killedVillagers = qr.rs.getInt("killedVillagers");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return killedVillagers;
	}
	
	public static int getPlayedGames(String p) throws Exception {
		int pgames = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM dm_stats WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			pgames = qr.rs.getInt("played_games");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return pgames;
	}
	
	public static int getVictories(String p) throws Exception {
		int victories = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM dm_stats WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			victories = qr.rs.getInt("victories");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return victories;
	}
	
	public static void addDeaths(String p) throws Exception {
		int deaths = Deathmatch.getDeaths(p);
		
		deaths++; // +1
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `dm_stats` SET `deaths` = " + deaths + " WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public static void addKills(String p) throws Exception {
		int kills = Deathmatch.getKills(p);
		
		kills++; // +1
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `dm_stats` SET `kills` = " + kills + " WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public static void addPoints(String p, int np) throws Exception {
		int points = Deathmatch.getPoints(p);
		
		int newPoints = points + np;
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `dm_stats` SET `points` = " + newPoints + " WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
		
		Bukkit.getPlayer(p).sendMessage(prefix + "�aDu hast �e" + np + " �aPunkte erhalten.");
	}
	
	public static void addWinPoints(String p) throws Exception {
		int points = Deathmatch.getPoints(p);
		
		int newpoints = points + 50;
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `dm_stats` SET `points` = " + newpoints + " WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public static void addVictory(String p) throws Exception {
		int victory = Deathmatch.getVictories(p);
		
		victory++; // +1
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `dm_stats` SET `victories` = " + victory + " WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public static void addKilledVillager(String p) throws Exception {
		int killedVillagers = Deathmatch.getKilledVillagers(p);
		
		killedVillagers++; // +1
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `dm_stats` SET `killedVillagers` = " + killedVillagers + " WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public static void addPlayedGames(String p) throws Exception {
		int pgames = Deathmatch.getPlayedGames(p);
		
		pgames++; // +1
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `dm_stats` SET `played_games` = " + pgames + " WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public static void removePoints(String p, int np) throws Exception {
		int points = Deathmatch.getPoints(p);
		
		int newPoints = points - np;
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `dm_stats` SET `points` = " + newPoints + " WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
		
		Bukkit.getPlayer(p).sendMessage(prefix + "�cDir wurden �e" + np + " �cPunkte abgezogen.");
	}
	
	public static void syncData(Player p) throws Exception {
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `dm_stats` SET `uuid` = '" + PlayerUtilities.getUUID(p.getName()) + "' WHERE username='" + p.getName() + "'");
			HandlerStorage.getHandler("main").execute("UPDATE `dm_stats` SET `username` = '" + p.getName() + "' WHERE uuid='" + PlayerUtilities.getUUID(p.getName()) + "'");
			System.out.println("[DM] Data sync succeded! Parameters: " + p.getName());
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public static void isInDatabase(String p) throws Exception {
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM dm_stats WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();

		if (qr.rs.getRow() == 0)
	      {
	        System.err.println("User not Found!");
	        HandlerStorage.getHandler("main").execute("INSERT INTO `dm_stats` (`id` ,`uuid` ,`username` ,`kills` ,`deaths` ,`points`)VALUES (NULL , '" + PlayerUtilities.getUUID(p) + "', '" + p + "', '0', '0', '100');");
	      }
		
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
	}
	
}
