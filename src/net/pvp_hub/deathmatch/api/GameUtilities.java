package net.pvp_hub.deathmatch.api;

import java.io.IOException;
import java.util.ArrayList;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.GameState;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.deathmatch.Deathmatch;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

public class GameUtilities {
	
	private static Deathmatch plugin;
	public GameUtilities(Deathmatch plugin){
		this.plugin = plugin;
	}

	public static ArrayList<Location> redstoneBlocks = new ArrayList<Location>();
	public static ArrayList<Location> goldenBlocks = new ArrayList<Location>();
	public static ArrayList<Location> diamondBlocks = new ArrayList<Location>();
	
	public static void endGame(Team t){
		if(t == Deathmatch.greenTeam){
			plugin.friendly = true;
			Bukkit.broadcastMessage("");
			Bukkit.broadcastMessage("�2�lTeam GR�N hat das FIRST-TO-12 Deathmatch GEWONNEN!");
			Bukkit.broadcastMessage("");
			
			PvPHubCore.setGameState(GameState.ENDING);
			
			for(OfflinePlayer g : plugin.greenTeam.getPlayers()){
				try {
					PlayerUtilities.addCoins((Player)g, 100);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					Deathmatch.addPoints(g.getName(), 50);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					Deathmatch.addVictory(g.getName());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			Bukkit.getScheduler().scheduleSyncDelayedTask(plugin.getInstance(), new Runnable(){
				public void run(){
					for(Player all : Bukkit.getOnlinePlayers()){
						try {
							PlayerUtilities.connect(plugin.getInstance(), "lobby", all);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					
					GameUtilities.refillDiamond();
					GameUtilities.refillRedstone();
					GameUtilities.refillGold();
					
					Bukkit.reload();
				}
			}, 5 * 20);
		}
		
		if(t == Deathmatch.blueTeam){
			plugin.friendly = true;
			Bukkit.broadcastMessage("");
			Bukkit.broadcastMessage("�9�lTeam BLAU hat das FIRST-TO-12 Deathmatch GEWONNEN!");
			Bukkit.broadcastMessage("");
			
			PvPHubCore.setGameState(GameState.ENDING);
			
			for(OfflinePlayer g : plugin.blueTeam.getPlayers()){
				try {
					PlayerUtilities.addCoins((Player)g, 100);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					Deathmatch.addPoints(g.getName(), 50);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					Deathmatch.addVictory(g.getName());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			Bukkit.getScheduler().scheduleSyncDelayedTask(plugin.getInstance(), new Runnable(){
				public void run(){
					for(Player all : Bukkit.getOnlinePlayers()){
						try {
							PlayerUtilities.connect(plugin.getInstance(), "lobby", all);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					
					GameUtilities.refillDiamond();
					GameUtilities.refillRedstone();
					GameUtilities.refillGold();
					
					Bukkit.reload();
				}
			}, 5 * 20);
		}
		
		if(t == Deathmatch.redTeam){
			plugin.friendly = true;
			Bukkit.broadcastMessage("");
			Bukkit.broadcastMessage("�4�lTeam ROT hat das FIRST-TO-12 Deathmatch GEWONNEN!");
			Bukkit.broadcastMessage("");
			
			PvPHubCore.setGameState(GameState.ENDING);
			
			for(OfflinePlayer g : plugin.redTeam.getPlayers()){
				try {
					PlayerUtilities.addCoins((Player)g, 100);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					Deathmatch.addPoints(g.getName(), 50);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					Deathmatch.addVictory(g.getName());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			Bukkit.getScheduler().scheduleSyncDelayedTask(plugin.getInstance(), new Runnable(){
				public void run(){
					for(Player all : Bukkit.getOnlinePlayers()){
						try {
							PlayerUtilities.connect(plugin.getInstance(), "lobby", all);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					
					GameUtilities.refillDiamond();
					GameUtilities.refillRedstone();
					GameUtilities.refillGold();
					
					Bukkit.reload();
				}
			}, 5 * 20);
		}
	}
	
	public static void refillRedstone(){
		Bukkit.broadcastMessage(Deathmatch.prefix + "Alle �4Redstone-Kisten �6wurden aufgef�llt!");
		for(Location loc : GameUtilities.redstoneBlocks){
			loc.getBlock().setType(Material.REDSTONE_BLOCK);
		}
		
		for(Player all : Bukkit.getOnlinePlayers()){
			all.playSound(all.getEyeLocation(), Sound.FIREWORK_LAUNCH, 1.0F, 1.0F);
		}
		
		GameUtilities.redstoneBlocks.clear();
	}
	
	public static void refillGold(){
		Bukkit.broadcastMessage(Deathmatch.prefix + "Alle �eGold-Kisten �6wurden aufgef�llt!");
		for(Location loc : GameUtilities.goldenBlocks){
			loc.getBlock().setType(Material.GOLD_BLOCK);
		}
		
		for(Player all : Bukkit.getOnlinePlayers()){
			all.playSound(all.getEyeLocation(), Sound.FIREWORK_LAUNCH, 1.0F, 1.0F);
		}
		
		GameUtilities.goldenBlocks.clear();
	}
	
	public static void refillDiamond(){
		Bukkit.broadcastMessage(Deathmatch.prefix + "Alle �bDiamant-Kisten �6wurden aufgef�llt!");
		for(Location loc : GameUtilities.diamondBlocks){
			loc.getBlock().setType(Material.DIAMOND_BLOCK);
		}
		
		for(Player all : Bukkit.getOnlinePlayers()){
			all.playSound(all.getEyeLocation(), Sound.FIREWORK_LAUNCH, 1.0F, 1.0F);
		}
		
		GameUtilities.diamondBlocks.clear();
	}
	
}
