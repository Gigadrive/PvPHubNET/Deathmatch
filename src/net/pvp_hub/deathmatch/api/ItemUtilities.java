package net.pvp_hub.deathmatch.api;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemUtilities {

	public static ItemStack namedItem(Material m, String name, String[] lore)
	  {
	    ItemStack is = new ItemStack(m);
	    ItemMeta meta = is.getItemMeta();
	    meta.setDisplayName(name);
	    meta.setLore(Arrays.asList(lore));
	    is.setItemMeta(meta);
	    return is;
	  }
	
}
