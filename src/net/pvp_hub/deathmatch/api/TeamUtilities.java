package net.pvp_hub.deathmatch.api;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import net.pvp_hub.deathmatch.Deathmatch;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

public class TeamUtilities {
	
	public static List<Team> teams = new LinkedList<Team>();
	public static List<Player> playersForBlue = new LinkedList<Player>();
	public static List<Player> playersForGreen = new LinkedList<Player>();
	public static List<Player> playersForRed = new LinkedList<Player>();

	// 0 = Team 1 (Red) | 1 = Team 2 (Blue) | 2 = Team 3 (Green)
	
	public static String getTeamColor(Team team){
		if(team.getName().equals("BLUE")){
			return "�9";
		} else if(team.getName().equals("RED")){
			return "�4";
		} else if(team.getName().equals("GREEN")){
			return "�2";
		} else {
			return null;
		}
	}
	
	public static Team getTeamByPlayer(Player p){
		if(Deathmatch.blueTeam.getPlayers().contains(p)){
			return Deathmatch.blueTeam;
		} else if(Deathmatch.redTeam.getPlayers().contains(p)){
			return Deathmatch.redTeam;
		} else if(Deathmatch.greenTeam.getPlayers().contains(p)){
			return Deathmatch.greenTeam;
		} else {
			//System.out.println("USER ISNT IN TEAM!");
			return null;
		}
	}
	
	public static boolean isInTeam(Player p){
		if(TeamUtilities.getTeamByPlayer(p) != null){
			return true;
		} else {
			return false;
		}
	}
	
	public static Team getRandomTeam(int i){
		Collections.shuffle(TeamUtilities.teams);
		return teams.get(0);
	}
	
	public static void shuffleIntoTeams(){
		int i = 0;
		
		for (Player all : Bukkit.getOnlinePlayers()){
			if(!isInTeam(all)){
				i++;
				TeamUtilities.getRandomTeam(i).addPlayer(all);
			}
		}
	}
	
	public static void removePlayerFromTeam(Player p){
		TeamUtilities.getTeamByPlayer(p).removePlayer(p);
	}
	
	public static void teleportOnMap(Player p, Team team){
		String t = null;
		if(team.getName().equals("BLUE")){
			t = "Blue";
		} else if(team.getName().equals("GREEN")){
			t = "Green";
		} else if(team.getName().equals("RED")){
			t = "Red";
		}
		World w = Bukkit.getWorld(Deathmatch.cfg.getString("DM.Spawns." + t + ".World"));
		double x = Deathmatch.cfg.getDouble("DM.Spawns." + t + ".X");
		double y = Deathmatch.cfg.getDouble("DM.Spawns." + t + ".Y");
		double z = Deathmatch.cfg.getDouble("DM.Spawns." + t + ".Z");
		float yaw = Deathmatch.cfg.getInt("DM.Spawns." + t + ".Yaw");
		float pitch = Deathmatch.cfg.getInt("DM.Spawns." + t + ".Pitch");
		
		Location loc = new Location(w, x, y, z, yaw, pitch);
		p.teleport(loc);
	}
	
}
