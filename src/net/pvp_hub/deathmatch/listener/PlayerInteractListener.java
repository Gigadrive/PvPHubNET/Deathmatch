package net.pvp_hub.deathmatch.listener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.deathmatch.Deathmatch;
import net.pvp_hub.deathmatch.api.GameUtilities;
import net.pvp_hub.deathmatch.inv.ChooseTeam;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PlayerInteractListener implements Listener {

	private Deathmatch plugin;
	public PlayerInteractListener(Deathmatch plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onSpecialItem(PlayerInteractEvent e){
		Player p = e.getPlayer();
		
		try {
			if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){
				if(p.getItemInHand().getType() == Material.GHAST_TEAR){
					p.getInventory().remove(Material.GHAST_TEAR);
					p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 20 * 20, 1));
					p.sendMessage(PluginMeta.prefix + "�aDu hast einen 20-Sek�ndigen �eUnsichtbarkeitseffekt �aerhalten.");
				}
			}
			
			if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){
				if(p.getItemInHand().getType() == Material.BLAZE_ROD){
					p.getInventory().remove(Material.BLAZE_ROD);
					p.setHealth(20.0);
					p.sendMessage(PluginMeta.prefix + "�aDu wurdest geheilt.");
				}
			}
			
			if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){
				if(p.getItemInHand().getType() == Material.WOOL){
					ChooseTeam.openFor(p);
				}
			}
		} catch (Exception e1){}
	}
	
	@EventHandler
	public void onRedChest(PlayerInteractEvent e){
		Player p = e.getPlayer();
		
		try {
			if(e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getType().equals(Material.REDSTONE_BLOCK)){
				if(e.getPlayer().getItemInHand().getType() == Material.AIR){
					p.sendMessage(plugin.prefix + "�cBitte nimm daf�r ein Item in die Hand");
				} else {
					Random rnd = new Random();
					int n = 1;
					n = rnd.nextInt(6);
					Inventory inv = Bukkit.createInventory(null, InventoryType.CHEST);
					List<ItemStack> items = new ArrayList<ItemStack>();
					
					items.add(new ItemStack(Material.STONE_SWORD));
					items.add(new ItemStack(Material.BOW));
					items.add(new ItemStack(Material.ARROW, 32));
					items.add(new ItemStack(Material.LEATHER_HELMET));
					items.add(new ItemStack(Material.LEATHER_CHESTPLATE));
					items.add(new ItemStack(Material.LEATHER_LEGGINGS));
					items.add(new ItemStack(Material.LEATHER_BOOTS));
					
					Collections.shuffle(items);
					
					p.getInventory().addItem(items.get(0));
					
					e.getClickedBlock().setType(Material.AIR);
					GameUtilities.redstoneBlocks.add(e.getClickedBlock().getLocation());
				}
			}
		} catch(Exception e1){}
	}
	
	@EventHandler
	public void onGoldChest(PlayerInteractEvent e){
		Player p = e.getPlayer();
		
		try {
			if(e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getType().equals(Material.GOLD_BLOCK)){
				if(e.getPlayer().getItemInHand().getType() == Material.AIR){
					p.sendMessage(plugin.prefix + "�cBitte nimm daf�r ein Item in die Hand");
				} else {
					Random rnd = new Random();
					int n = 1;
					n = rnd.nextInt(6);
					Inventory inv = Bukkit.createInventory(null, InventoryType.CHEST);
					List<ItemStack> items = new ArrayList<ItemStack>();
					
					items.add(new ItemStack(Material.IRON_SWORD));
					items.add(new ItemStack(Material.ARROW, 64));
					items.add(new ItemStack(Material.CHAINMAIL_HELMET));
					items.add(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
					items.add(new ItemStack(Material.CHAINMAIL_LEGGINGS));
					items.add(new ItemStack(Material.CHAINMAIL_BOOTS));
					
					Collections.shuffle(items);
					
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "give " + p.getName() + " " + items.get(0).getType().toString() + " 1");
					
					e.getClickedBlock().setType(Material.AIR);
					GameUtilities.goldenBlocks.add(e.getClickedBlock().getLocation());
				}
				
			}
		} catch(Exception e1){}
	}
	
	@EventHandler
	public void onDiamondChest(PlayerInteractEvent e){
		Player p = e.getPlayer();
		
		try {
			if(e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getType().equals(Material.DIAMOND_BLOCK)){
				if(e.getPlayer().getItemInHand().getType() == Material.AIR){
					p.sendMessage(plugin.prefix + "�cBitte nimm daf�r ein Item in die Hand");
				} else {
					Random rnd = new Random();
					int n = 1;
					n = rnd.nextInt(6);
					Inventory inv = Bukkit.createInventory(null, InventoryType.CHEST);
					List<ItemStack> items = new ArrayList<ItemStack>();
					
					items.add(new ItemStack(Material.DIAMOND_SWORD));
					items.add(new ItemStack(Material.IRON_HELMET));
					items.add(new ItemStack(Material.IRON_CHESTPLATE));
					items.add(new ItemStack(Material.IRON_LEGGINGS));
					items.add(new ItemStack(Material.IRON_BOOTS));
					
					Collections.shuffle(items);
					
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "give " + p.getName() + " " + items.get(0).getType().toString() + " 1");
					
					e.getClickedBlock().setType(Material.AIR);
					GameUtilities.diamondBlocks.add(e.getClickedBlock().getLocation());
					
					Bukkit.broadcastMessage(plugin.prefix + p.getDisplayName() + " �6hat in einer �bDiamant-Kiste �e" + items.get(0).getType().toString() + " �6gefunden!");
				}
				
			}
		} catch(Exception e1){}
	}
	
}
