package net.pvp_hub.deathmatch.listener;

import net.pvp_hub.deathmatch.Deathmatch;
import net.pvp_hub.deathmatch.api.TeamUtilities;
import net.pvp_hub.deathmatch.inv.VillagerShop;

import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class EntityInteractListener implements Listener {

	private Deathmatch plugin;
	public EntityInteractListener(Deathmatch plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEntityEvent e){
		final Player p = e.getPlayer();
		try{
			
			if(e.getRightClicked() instanceof Villager){
				Villager v = (Villager)e.getRightClicked();
				e.setCancelled(true);
				if(v.getCustomName().equals("�4[ �cTeam-Shop �4]")){
					if(TeamUtilities.getTeamByPlayer(p) == plugin.redTeam){
						VillagerShop.openFor(p);
					} else {
						p.sendMessage(plugin.prefix + "�cDas ist nicht dein Villager!");
						p.sendMessage(plugin.prefix + "�4�lT�TE IHN!!");
					}
				}
				
				if(v.getCustomName().equals("�9[ �3Team-Shop �9]")){
					if(TeamUtilities.getTeamByPlayer(p) == plugin.blueTeam){
						VillagerShop.openFor(p);
					} else {
						p.sendMessage(plugin.prefix + "�cDas ist nicht dein Villager!");
						p.sendMessage(plugin.prefix + "�4�lT�TE IHN!!");
					}
				}
				
				if(v.getCustomName().equals("�2[ �aTeam-Shop �2]")){
					if(TeamUtilities.getTeamByPlayer(p) == plugin.greenTeam){
						VillagerShop.openFor(p);
					} else {
						p.sendMessage(plugin.prefix + "�cDas ist nicht dein Villager!");
						p.sendMessage(plugin.prefix + "�4�lT�TE IHN!!");
					}
				}
			}
			
		} catch (Exception e1){}
	}
}
