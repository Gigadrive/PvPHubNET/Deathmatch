package net.pvp_hub.deathmatch.listener;

import net.pvp_hub.deathmatch.Deathmatch;
import net.pvp_hub.deathmatch.api.TeamUtilities;
import net.pvp_hub.deathmatch.inv.VillagerShop;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class EntityDamageByEntityListener implements Listener {

	private Deathmatch plugin;
	public EntityDamageByEntityListener(Deathmatch plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onDamageByEntity(EntityDamageByEntityEvent e){
		Player p = (Player)e.getDamager();
		try {
			if(e.getEntity() instanceof Villager){
				Villager v = (Villager)e.getEntity();
				
				if(v.getCustomName().equals("�4[ �cTeam-Shop �4]")){
					if(TeamUtilities.getTeamByPlayer(p) == plugin.redTeam){
						e.setCancelled(true);
						p.sendMessage(plugin.prefix + "�cDu kannst deinen eigenen Villager nicht attackieren!");
					} else {
						e.setCancelled(false);
						for(OfflinePlayer all : plugin.redTeam.getPlayers()){
							Player a = (Player)all;
							a.sendMessage(plugin.prefix + "�4Dein Villager wird attackiert!");
						}
					}
				}
				
				if(v.getCustomName().equals("�9[ �3Team-Shop �9]")){
					if(TeamUtilities.getTeamByPlayer(p) == plugin.blueTeam){
						e.setCancelled(true);
						p.sendMessage(plugin.prefix + "�cDu kannst deinen eigenen Villager nicht attackieren!");
					} else {
						e.setCancelled(false);
						for(OfflinePlayer all : plugin.blueTeam.getPlayers()){
							Player a = (Player)all;
							a.sendMessage(plugin.prefix + "�4Dein Villager wird attackiert!");
						}
					}
				}
				
				if(v.getCustomName().equals("�2[ �aTeam-Shop �2]")){
					if(TeamUtilities.getTeamByPlayer(p) == plugin.greenTeam){
						e.setCancelled(true);
						p.sendMessage(plugin.prefix + "�cDu kannst deinen eigenen Villager nicht attackieren!");
					} else {
						e.setCancelled(false);
						for(OfflinePlayer all : plugin.greenTeam.getPlayers()){
							Player a = (Player)all;
							a.sendMessage(plugin.prefix + "�4Dein Villager wird attackiert!");
						}
					}
				}
			}
		} catch (Exception e1){}
	}
	
}
