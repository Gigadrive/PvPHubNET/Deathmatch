package net.pvp_hub.deathmatch.listener;

import net.pvp_hub.deathmatch.Deathmatch;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

public class WeatherChangeListener implements Listener {

	private Deathmatch plugin;
	public WeatherChangeListener(Deathmatch plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onChange(WeatherChangeEvent e){
		e.setCancelled(true);
	}
	
}
