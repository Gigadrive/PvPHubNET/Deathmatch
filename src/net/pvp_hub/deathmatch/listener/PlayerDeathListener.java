package net.pvp_hub.deathmatch.listener;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.deathmatch.Deathmatch;
import net.pvp_hub.deathmatch.api.GameUtilities;
import net.pvp_hub.deathmatch.api.TeamUtilities;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerDeathListener implements Listener {

	private Deathmatch plugin;
	public PlayerDeathListener(Deathmatch plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		Player p = e.getEntity();
		e.getDrops().clear();
		e.setDroppedExp(0);
		if(p.getKiller() == null){
			e.setDeathMessage(plugin.prefix + p.getDisplayName() + " �6ist gestorben.");
		} else {
			Player killer = p.getKiller();
			e.setDeathMessage(plugin.prefix + p.getDisplayName() + " �6wurde von " + killer.getDisplayName() + " �6get�tet!");
			try {
				PlayerUtilities.addCoins(killer, 2);
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
			try {
				Deathmatch.addKills(killer.getName());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try {
				Deathmatch.addPoints(killer.getName(), 5);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try {
				Deathmatch.removePoints(p.getName(), 5);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try {
				Deathmatch.addDeaths(p.getName());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(TeamUtilities.getTeamByPlayer(killer).getName().equals("GREEN")){
				int s = plugin.greenScore.getScore();
				s++;
				plugin.greenScore.setScore(s);
				
				for(Player all : Bukkit.getOnlinePlayers()){
					all.setScoreboard(plugin.board);
				}
				
				if(plugin.greenScore.getScore() == 12){
					GameUtilities.endGame(Deathmatch.greenTeam);
				}
			}
			
			if(TeamUtilities.getTeamByPlayer(killer).getName().equals("RED")){
				int s = plugin.redScore.getScore();
				s++;
				plugin.redScore.setScore(s);
				
				for(Player all : Bukkit.getOnlinePlayers()){
					all.setScoreboard(plugin.board);
				}
				
				if(plugin.redScore.getScore() == 12){
					GameUtilities.endGame(Deathmatch.redTeam);
				}
			}
			
			if(TeamUtilities.getTeamByPlayer(killer).getName().equals("BLUE")){
				int s = plugin.blueScore.getScore();
				s++;
				plugin.blueScore.setScore(s);
				
				for(Player all : Bukkit.getOnlinePlayers()){
					all.setScoreboard(plugin.board);
				}
				
				if(plugin.blueScore.getScore() == 12){
					GameUtilities.endGame(Deathmatch.blueTeam);
				}
			}
		}
	}
	
}
