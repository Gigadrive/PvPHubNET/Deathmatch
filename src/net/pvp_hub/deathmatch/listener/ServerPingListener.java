package net.pvp_hub.deathmatch.listener;

import net.pvp_hub.deathmatch.Deathmatch;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class ServerPingListener implements Listener {

	private Deathmatch plugin;
	public ServerPingListener(Deathmatch plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPing(ServerListPingEvent e){
		e.setMotd(Deathmatch.gameState);
	}
	
}
