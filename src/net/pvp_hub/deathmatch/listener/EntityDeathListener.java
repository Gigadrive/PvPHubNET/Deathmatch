package net.pvp_hub.deathmatch.listener;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.deathmatch.Deathmatch;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class EntityDeathListener implements Listener {

	private Deathmatch plugin;
	public EntityDeathListener(Deathmatch plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onDeath(EntityDeathEvent e){
		Entity en = e.getEntity();
		if(en instanceof Villager){
			Villager v = (Villager)en;
			if(v.getCustomName().equals("�4[ �cTeam-Shop �4]")){
				for(OfflinePlayer all : plugin.redTeam.getPlayers()){
					Player a = (Player)all;
					a.sendMessage(plugin.prefix + "�cDein Villager wurde von " + e.getEntity().getKiller().getDisplayName() + " �cget�tet!");
					plugin.redIsKilled = true;
				}
				
				try {
					PlayerUtilities.addCoins(e.getEntity().getKiller(), 20);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					Deathmatch.addPoints(e.getEntity().getKiller().getName(), 5);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					Deathmatch.addKilledVillager(e.getEntity().getKiller().getName());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			if(v.getCustomName().equals("�9[ �3Team-Shop �9]")){
				for(OfflinePlayer all : plugin.blueTeam.getPlayers()){
					Player a = (Player)all;
					a.sendMessage(plugin.prefix + "�cDein Villager wurde von " + e.getEntity().getKiller().getDisplayName() + " �cget�tet!");
					plugin.blueIsKilled = true;
				}
				
				try {
					PlayerUtilities.addCoins(e.getEntity().getKiller(), 20);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					Deathmatch.addPoints(e.getEntity().getKiller().getName(), 5);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					Deathmatch.addKilledVillager(e.getEntity().getKiller().getName());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			if(v.getCustomName().equals("�2[ �aTeam-Shop �2]")){
				for(OfflinePlayer all : plugin.greenTeam.getPlayers()){
					Player a = (Player)all;
					a.sendMessage(plugin.prefix + "�cDein Villager wurde von " + e.getEntity().getKiller().getDisplayName() + " �cget�tet!");
					plugin.greenIsKilled = true;
				}
				
				try {
					PlayerUtilities.addCoins(e.getEntity().getKiller(), 20);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					Deathmatch.addPoints(e.getEntity().getKiller().getName(), 5);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				try {
					Deathmatch.addKilledVillager(e.getEntity().getKiller().getName());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}
	
}
