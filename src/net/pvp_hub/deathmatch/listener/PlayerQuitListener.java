package net.pvp_hub.deathmatch.listener;

import net.pvp_hub.deathmatch.Deathmatch;
import net.pvp_hub.deathmatch.api.GameUtilities;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

	private Deathmatch plugin;
	public PlayerQuitListener(Deathmatch plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		e.setQuitMessage(e.getPlayer().getDisplayName() + " �6hat den Server verlassen.");
		
		if(!Deathmatch.joinable){
			if(Deathmatch.blueTeam.getPlayers().size() == 0 && Deathmatch.greenTeam.getPlayers().size() == 0){
				GameUtilities.endGame(Deathmatch.redTeam);
			} else if(Deathmatch.redTeam.getPlayers().size() == 0 && Deathmatch.greenTeam.getPlayers().size() == 0){
				GameUtilities.endGame(Deathmatch.blueTeam);
			} else if(Deathmatch.redTeam.getPlayers().size() == 0 && Deathmatch.blueTeam.getPlayers().size() == 0){
				GameUtilities.endGame(Deathmatch.greenTeam);
			}
		}
	}
	
}
