package net.pvp_hub.deathmatch.listener;

import net.pvp_hub.deathmatch.Deathmatch;
import net.pvp_hub.deathmatch.api.TeamUtilities;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class AsyncPlayerChatListener implements Listener {

	private Deathmatch plugin;
	public AsyncPlayerChatListener(Deathmatch plugin){
		this.plugin = plugin;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onChat(AsyncPlayerChatEvent e){
		Player p = e.getPlayer();
		
		if(TeamUtilities.isInTeam(p)){
			if(TeamUtilities.getTeamByPlayer(p).getName().equals("BLUE")){
				e.setFormat("�8<[�9Team BLUE�8] " + p.getDisplayName() + "�8> �f" + e.getMessage());
			} else if(TeamUtilities.getTeamByPlayer(p).getName().equals("RED")){
				e.setFormat("�8<[�4Team RED�8] " + p.getDisplayName() + "�8> �f" + e.getMessage());
			} else if(TeamUtilities.getTeamByPlayer(p).getName().equals("GREEN")){
				e.setFormat("�8<[�2Team GREEN�8] " + p.getDisplayName() + "�8> �f" + e.getMessage());
			} else {
				p.kickPlayer("�cFehler.");
			}
		}
	}
	
}
