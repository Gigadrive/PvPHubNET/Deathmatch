package net.pvp_hub.deathmatch.listener;

import net.pvp_hub.deathmatch.Deathmatch;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakListener implements Listener {

	private Deathmatch plugin;
	public BlockBreakListener(Deathmatch plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent e){
		e.setCancelled(true);
	}
	
}
