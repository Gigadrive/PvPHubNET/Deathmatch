package net.pvp_hub.deathmatch.listener;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.deathmatch.Deathmatch;
import net.pvp_hub.deathmatch.api.ItemUtilities;
import net.pvp_hub.deathmatch.api.TeamUtilities;

import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryClickListener implements Listener {

	private Deathmatch plugin;
	public InventoryClickListener(Deathmatch plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e){
		Player p = (Player)e.getWhoClicked();
		try {
			if(e.getInventory().getName().equals("Team-Shop")){
				if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�5�lEnderperle")){
					e.setCancelled(true);
					p.closeInventory();
					try {
						if(PlayerUtilities.getCoins(p) < 100){
							p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Coins.");
						} else {
							PlayerUtilities.removeCoins(p, 100);
							p.getInventory().addItem(ItemUtilities.namedItem(Material.ENDER_PEARL, "�5�lEnderperle", new String[]{"�7Wirf sie und teleportiere dich an", "�7die Stelle, wo die Enderperle aufkommt."}));
							p.closeInventory();
							p.sendMessage(plugin.prefix + "�aDu hast �e1 Enderperle �aerhalten.");
						}
					} catch (Exception e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
				}
				
				else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�9�lUnsichtbarkeit")){
					e.setCancelled(true);
					p.closeInventory();
					try {
						if(PlayerUtilities.getCoins(p) < 250){
							p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Coins.");
						} else {
							PlayerUtilities.removeCoins(p, 250);
							p.getInventory().addItem(ItemUtilities.namedItem(Material.GHAST_TEAR, "�9�lUnsichtbarkeit", new String[]{"�7Benutz es und erhalte einen", "�720-sek�ndigen Unsichtbarkeitseffekt", "�cKosten: �e250 Coins"}));
							p.closeInventory();
							p.sendMessage(plugin.prefix + "�aDu hast �e1 Unsichbarmacher �aerhalten.");
						}
					} catch (Exception e2){}
				}
				
				else if(e.getCurrentItem().getItemMeta().getDisplayName().equals("�4�lHealth-Package")){
					e.setCancelled(true);
					p.closeInventory();
					try {
						if(PlayerUtilities.getCoins(p) < 60){
							p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Coins.");
						} else {
							PlayerUtilities.removeCoins(p, 60);
							p.getInventory().addItem(ItemUtilities.namedItem(Material.BLAZE_ROD, "�4�lHealth-Package", new String[]{"�7Benutz es und regeneriere", "�7dein Leben komplett voll", "�cKosten: �e60 Coins"}));
							p.closeInventory();
							p.sendMessage(plugin.prefix + "�aDu hast �e1 Health-Package �aerhalten.");
						}
					} catch (Exception e2){}
				}
				
				else {
					e.setCancelled(true);
					p.closeInventory();
				}
			}
			
			if(e.getInventory().getName().equals("�2Team-Wahl")){
				if(e.getCurrentItem().getDurability() == (short) 14){
					// rot
					e.setCancelled(true);
					p.closeInventory();
					
					if(PlayerUtilities.getChips(p) < 1){
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Chips.");
					} else {
						PlayerUtilities.removeChips(p, 1);
						p.sendMessage(plugin.prefix + "�aDu wirst in Team �4Rot �aspielen.");
						if(TeamUtilities.playersForBlue.contains(p)){
							TeamUtilities.playersForBlue.remove(p);
						}
						
						if(TeamUtilities.playersForRed.contains(p)){
							TeamUtilities.playersForRed.remove(p);
						}
						
						if(TeamUtilities.playersForGreen.contains(p)){
							TeamUtilities.playersForGreen.remove(p);
						}
						
						TeamUtilities.playersForRed.add(p);
					}
				}
				
				else if(e.getCurrentItem().getDurability() == (short) 11){
					// blau
					e.setCancelled(true);
					p.closeInventory();
					
					if(PlayerUtilities.getChips(p) < 1){
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Chips.");
					} else {
						PlayerUtilities.removeChips(p, 1);
						p.sendMessage(plugin.prefix + "�aDu wirst in Team �9Blau �aspielen.");
						if(TeamUtilities.playersForBlue.contains(p)){
							TeamUtilities.playersForBlue.remove(p);
						}
						
						if(TeamUtilities.playersForRed.contains(p)){
							TeamUtilities.playersForRed.remove(p);
						}
						
						if(TeamUtilities.playersForGreen.contains(p)){
							TeamUtilities.playersForGreen.remove(p);
						}
						
						TeamUtilities.playersForBlue.add(p);
					}
				}
				
				else if(e.getCurrentItem().getDurability() == (short) 5){
					// gr�n
					e.setCancelled(true);
					p.closeInventory();
					
					if(PlayerUtilities.getChips(p) < 1){
						p.sendMessage(PluginMeta.prefix + "�cDu hast nicht genug Chips.");
					} else {
						PlayerUtilities.removeChips(p, 1);
						p.sendMessage(plugin.prefix + "�aDu wirst in Team �2Gr�n �aspielen.");
						if(TeamUtilities.playersForBlue.contains(p)){
							TeamUtilities.playersForBlue.remove(p);
						}
						
						if(TeamUtilities.playersForRed.contains(p)){
							TeamUtilities.playersForRed.remove(p);
						}
						
						if(TeamUtilities.playersForGreen.contains(p)){
							TeamUtilities.playersForGreen.remove(p);
						}
						
						TeamUtilities.playersForGreen.add(p);
					}
				}
				
				else {
					e.setCancelled(true);
					p.closeInventory();
				}
			}
			
		} catch(Exception e1){}
	}
	
}
