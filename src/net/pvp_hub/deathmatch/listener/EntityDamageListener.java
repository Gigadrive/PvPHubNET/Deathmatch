package net.pvp_hub.deathmatch.listener;

import net.pvp_hub.deathmatch.Deathmatch;

import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class EntityDamageListener implements Listener {

	private Deathmatch plugin;
	public EntityDamageListener(Deathmatch plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent e){
		if(e.getEntity() instanceof Player){
			if(plugin.friendly){
				e.setCancelled(true);
			}
		}
		
		if(e.getEntity() instanceof Villager){
			e.setCancelled(true);
		}
	}
	
}
