package net.pvp_hub.deathmatch.listener;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.deathmatch.Deathmatch;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PlayerJoinListener implements Listener {

	private Deathmatch plugin;
	public PlayerJoinListener(Deathmatch plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		final Player p = e.getPlayer();
		
		e.setJoinMessage(null);
		if(plugin.joinable){
			Location loc = Bukkit.getWorld("Lobby").getSpawnLocation();
			p.teleport(loc);
			
			Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
				public void run(){
					Bukkit.broadcastMessage(p.getDisplayName() + " �6hat den Server betreten");
					p.sendMessage("�e[MAP] Name: �b" + plugin.mapName);
					p.sendMessage("�e[MAP] Von: �b" + plugin.mapBuilder);
					p.sendMessage("�e[MAP] Link: �b" + plugin.mapLink);
					PlayerUtilities.clearInventory(p);
					p.setHealth(20.0);
					p.setFoodLevel(20);
					p.setFireTicks(0);
					p.setFlying(false);
					
					ItemStack ch = new ItemStack(Material.WOOL);
					ItemMeta chM = ch.getItemMeta();
					chM.setDisplayName("�aTeam-Wahl");
					ch.setItemMeta(chM);
					p.getInventory().setItem(8, ch);
					
					Location loc = new Location(Bukkit.getWorld("Lobby"), -1083, 24, -145);
					p.teleport(loc);
					
					try {
						Deathmatch.isInDatabase(p.getName());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					try {
						Deathmatch.syncData(p);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					//p.setScoreboard(plugin.board);
				}
			});
			try {
				//plugin.isInDatabase(p.getName());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else {
			p.kickPlayer(Deathmatch.prefix + "�cDas Spiel hat bereits begonnen.");
		}
	}
	
}
