package net.pvp_hub.deathmatch.listener;

import net.pvp_hub.deathmatch.Deathmatch;

import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

public class CreatureSpawnListener implements Listener {

	private Deathmatch plugin;
	public CreatureSpawnListener(Deathmatch plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onSpawn(CreatureSpawnEvent e){
		if(e.getEntity() instanceof Villager){
			e.setCancelled(false);
		} else {
			e.setCancelled(true);
		}
	}
	
}
