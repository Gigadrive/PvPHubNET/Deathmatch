package net.pvp_hub.deathmatch.listener;

import net.pvp_hub.deathmatch.Deathmatch;
import net.pvp_hub.deathmatch.api.TeamUtilities;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerRespawnListener implements Listener {

	private Deathmatch plugin;
	public PlayerRespawnListener(Deathmatch plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e){
		final Player p = e.getPlayer();
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
			public void run(){
				TeamUtilities.teleportOnMap(p, TeamUtilities.getTeamByPlayer(p));
			}
		});
		
		p.getInventory().setItem(0, new ItemStack(Material.GOLD_SWORD));
	}
	
}
